# MISE EN PLACE DU PREMIER SITE

## Création de la base de données pour Wordpress

L'accès au serveur MariaDB se fait *via* le client `mysql`. Celui-ci doit être lancé en tant que root. En effet, seul root est autorisé à se connecter en root. Si l'utilitaire `sudo` est présent, il est à privilégier. Autrement, il fautra vous logger en root pour exécuter `mysql` (`su`) :
```
mysql
```


Ajout de la base de données :
```sql
CREATE DATABASE www CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ;
```
>En cas d'erreur, vous pouvez supprimer la base *via* la commande :
>```sql
>DROP DATABASE www ;
>```

Afin de maximiser la sécurité, un utilisateur sera dédié à l'utilisation de la base de données pour cette instance de Wordpress. Cet utilisateur ne pourra accéder qu'à cette seule base et ne pourra se connecter que depuis le serveur local.

Ajout d'un utilisateur MariaDB local :
```sql
CREATE USER 'wwwadmin'@localhost IDENTIFIED BY 'mot de passe' ;
```
>En cas d'erreur, vous pouvez supprimer l'utilisateur' *via* la commande :
>```sql
>DROP USER 'wwwadmin'@localhost ;
>```
Ajout des permissions de l'utilisateur sur la base de donées :
```sql
GRANT ALL PRIVILEGES ON www.* TO wwwadmin@localhost ;
```
> **NOTE**\
> La modification des privilèges *via* la commande `GRANT` prend effet immédiatement et ne nécessite pas le rechargement de la table des privilèges (`FLUSH PRIVILEGES`).\
\
>Référence : https://dev.mysql.com/doc/refman/5.7/en/privilege-changes.html

## Création du vhost Apache 2
---
Les définitions de vHost sont à placer dans un fichier de configuration additionnel dans le répertoire `/etc/apache2/sites-available` :
```
cd /etc/apache2/sites-available
touch www.pop.lan
```
```
nano www.pop.lan
```
>Le fichier est  présent sur le dépôt

Une fois configuré, on peut activer le site :
```
a2ensite www.pop.lan
```
>**NOTE**\
>L'activation créé un lien symbolique du fichier présent dans `/etc/apache2/sites-available` vers `/etc/apache2/sites-enabled`. Les fichiers présents dans ce dernier répertoire sont sourcés dans le fichier de configuration général `/etc/apache2/apache2.conf`.

>:red_circle: **ATTENTION**
>Pour faire les choses bien, si vous avez l'intention de configurer Wordpress sans utiliser l'assistant, n'activez pas le site maintenant. 

Une fois que c'est fait, il est bon de vérifier la configuration de Apache2 :
```
apache2ctl configtest
```
Enfin, si tout est OK, on peut recharger Apache2 sans risque :
```
systemctl reload apache2.service
```


## Installation de Wordpress
---
La mise en place des fichiers pour Wordpress est assez simple. Il suffit de se déplacer dans le répertoire d'accueil des sites :
```
cd /mnt/local1/www
```
Puis de télécharger l'archive de la dernière version et d'en extraire le contenu :
```
wget https://fr.wordpress.org/latest-fr_FR.tar.gz
tar xzf latest-fr_FR.tar.gz
```
Le répertoire ainsi extrait se nomme '`wordpress`'. Il faudra le renommer afin qu'il corresponde au chemin vers le document racine (`/mnt/local1/www/www.pop.lan`) :
```
mv wordpress www.pop.lan
```

Enfin, attribuer ces nouveaux fichiers à l'utilisateur qui fait tourner Apache2 :
```
chown -R www-data:www-data www.pop.lan
```
## Configuration de base
---
À présent, il est possible de terminer l'installation de Wordpress de deux façons :

- En HTTP, via l'assistant "graphique"
- En éditant le fichier de configuration

La dernière méthode présente quelques avantages :

- L'accès HTTP au site peut être désactivé le temps de l'installation (ce qui est plus propre et sûr)
- La configuration peut être préparée à l'anvance et même automatisée

## Configuration par édition du fichier `wp-config.php`
---

Le fichier de configuration principal de Worpdress est `wp-config.php`.
Dans les fichiers par défaut se trouve un modèle (`wp-config-sample.php`) qui servira de base à la création du fichier définitif.

Si ce nest pas le cas, déplacez-vous jusqu'au répertoire racine du site :
```
cd /mnt/local1/www/www.pop.lan
```
Copiez le fichier `wp-config-sample.php` vers `wp-config.php` puis ouvrez-le dans votre éditeur préféré :
```
mv wp-config-sample.php wp-config.php
nano wp-config.php
```
La configuration se fait au moyen de constantes. Dans un premier temps, configurez l'accès à la base de données :

<!-- ||
|:--:|
|`DB_NAME`| -->

```php
define('DB_NAME', 'www');
define('DB_USER', 'wwwadmin');
define('DB_PASSWORD', 'mot de passe');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8mb4');
```

Les constantes suivantes concernent les clés d'authtification et le salage des mots de passe sur l'instance courante. Ces valeurs doivent être re-générées. Un lien est présent en commentaire pour générer automatiquement ces paramètres :
https://api.wordpress.org/secret-key/1.1/salt/

Il vous suffira de remplacer les anciennes définitions par les nouvelles.
