# INSTALLATION DE APACHE

## Pré-requis
- L'enregistrement `lamp1 IN A 172.16.0.133` doit exister dans le DNS
- L'enregistrement `www IN A 172.16.0.134` doit exister dans le DNS
- L'enregistrement `@ IN A 172.16.0.134` doit exister dans le DNS


## Installer Apache2
```
apt install apache2
```


## Premier test
- Se rendre sur http://pop.lan.
- Se rendre sur http://www.pop.lan.
- Se rendre sur http://lamp1.pop.lan.

La page par défaut de Apache pour Debian doit s'afficher.

