# MISE EN PLACE DES FICHIERS POUR LES SITES

## Pré-requis
- La matrice RAID1 `/dev/md0p1` doit exister et être montée sur `/mnt/local1`

## Mise en place des répertoires racine
Création du répertoire d'accueil des racines
```
mkdir /mnt/local1/www
```
Pour que ça fonctionne, les fichiers doivent appartenir à l'utilisateur et au groupe qui font tourner Apache (`www-data`) :
```
chown www-data:www-data /mnt/local1/www
```
## Mise en place des logs pour chaque site
On créé un répertoire de log pour chaque site :
```
mkdir /var/log/apache2/www.pop.lan
```
Et on modifie les propriétaires, en accord avec la façon de faire de Debian. Les logs sont accessible en lecture au groupe `adm` :

```
chown root:adm /var/log/apache2/www.pop.lan
chmod 750 /var/log/apache2/www.pop.lan
```
Enfin, il ne faut pas oublier de "roter" (rotater ? Bref... je n'ai pas trouvé de traduction satisfaisante, désolé) les logs créés sous peine de saturer la partition `/var`.

Cette action est prise en charge par le service `logrotate`. Pour ce faire, nous allons simplement reprendre la configuration installée par défaut pour Apache2 et la modifier :
```
cd /etc/logrotate.d
cp apache2 www.pop.lan
```
```
nano www.pop.lan
```
> Le fichier est disponible sur le dépôt
