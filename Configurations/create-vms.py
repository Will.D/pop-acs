#! /usr/bin/env python3

import os
import sys
import platform
import shutil



vms = ['R1', 'FW1', 'INFRA1','LAMP1']
hdds = []
error = False
errorMessages = []
ansi_green = ''
ansi_red   = ''
ansi_orange = ''
ansi_reset = ''


vboxBaseFolder = 'VirtualBox'
vboxGroupName = 'POP School ACS'


workdir = os.getcwd()

# print( os.path.basename(sys.argv[0]) )
# print( os.path.dirname( os.path.abspath(sys.argv[0])) )


###
### PLATFORM CHECK
###
if platform.system() == 'Windows':
	separator = '\\'
	homedir = os.getenv('USERPROFILE')
	username = os.getenv('USERNAME')
	oldpath = os.getenv('PATH')
	os.environ['PATH'] = str( oldpath + ';C:\\Program Files\\Oracle\\VirtualBox\\')
	vboxmanage = 'VBoxManage.exe'

	# kernel32 = __import__("ctypes").windll.kernel32
	# kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)
	# del kernel32


elif platform.system() == 'Linux':
	separator = '/'
	homedir = os.getenv('HOME')
	username = os.getenv('USER')
	vboxmanage = '/usr/bin/vboxmanage' ## Ubuntu/Mint

	ansi_green = '\033[0;32m'
	ansi_red   = '\033[0;31m'
	ansi_orange = '\033[0;93m'
	ansi_reset = '\033[00m'

else:
	print('Error: Unsupported platform', file=sys.stderr)
	sys.exit(1)

myDir = os.path.dirname( os.path.abspath(sys.argv[0]))
workdir = myDir



###
### SOURCE FILES AND DEPENDENCIES CHECK
###
## Check directories
for file in (homedir,workdir):
	if not os.path.exists(file):
		error = True
		errorMessages.append(f'"{file}": No such directory')
	if not os.path.isdir(file):
		error = True
		errorMessages.append(f'"{file}": Not a directory')

## Check vboxmanage
## TODO

## Build hard drives list
for file in vms:
	hdds.append(f'{workdir}{separator}{file}.vdi')
hdds.append(f'{workdir}{separator}RAID-Disk-01.vdi')
hdds.append(f'{workdir}{separator}RAID-Disk-02.vdi')

## Check VM hard drives
for file in hdds:
	if not os.path.exists(file):
		error = True
		errorMessages.append(f'"{file}": File not found')
	elif not os.path.isfile(file):
		error = True
		errorMessages.append(f'"{file}": Not a file')
	else:
		print(f'[ {ansi_green}FOUND{ansi_reset} ] "{file}"')

if error:
	for message in errorMessages:
		print(f'[ {ansi_red}ERROR{ansi_reset} ] {message}', file=sys.stderr)
	sys.exit(1)








###
### VIRTUALBOX CONFIG AND FILES CHECK
###
## Create basedir
file = f'{homedir}{separator}{vboxBaseFolder}'
if os.path.exists(file):
	print(f'[ {ansi_green}FOUND{ansi_reset} ] "{file}" (VirtualBox Base dir)')
else:
	print(f'[ {ansi_orange}WARN {ansi_reset} ] "{file}" (VirtualBox Base dir) Not found, creating...')
	## Try ; catch
	os.mkdir(file)

## Create Structure dirs
basedir = f'{homedir}{separator}{vboxBaseFolder}'
for file in ('HDD', 'VM', 'ISO'):
	if os.path.exists(f'{basedir}{separator}{file}'):
		print(f'[ {ansi_green}FOUND{ansi_reset} ] "{basedir}{separator}{file}"')
	else:
		print(f'[ {ansi_orange}WARN {ansi_reset} ] "{basedir}{separator}{file}" Not found, creating...')
		## Try ; catch
		os.mkdir(f'{basedir}{separator}{file}')



###
### LET'S GO
###
## Copy hard drives images
for file in hdds:
	dest = f'{basedir}{separator}HDD{separator}{os.path.basename(file)}'
	if os.path.exists(dest):
		print(f'[ {ansi_green}FOUND{ansi_reset} ] "{dest}"')
	else:
		print(f'Copying "{dest}"')
		# try ; catch
		shutil.copy2(file,dest)


### Create virtualbox group
dest = f'{basedir}{separator}VM{separator}{vboxGroupName}'
if not os.path.exists(dest):
	print(f'[ {ansi_orange}WARN {ansi_reset} ] "{dest}" Not found, creating...')
	## Try ; catch
	os.mkdir(dest)
else:
	print(f'[ {ansi_green}FOUND{ansi_reset} ] "{dest}"')
	

### Create VMs
for vm in vms:

	print(f'\n[ {ansi_green}INFO{ansi_reset} ] Creating Virtual Machine "{vm}"')
	os.system( f'{vboxmanage} createvm --name "{vm}" --ostype Debian_64 --groups "/{vboxGroupName}" --basefolder "{basedir}{separator}/VM" --register')

	print(f'[ {ansi_green}INFO{ansi_reset} ] "{vm}": Setting up memory and video settings')
	os.system( f'{vboxmanage} modifyvm "{vm}" --memory 2048 --vram 16 --graphicscontroller vmsvga' )

	print(f'[ {ansi_green}INFO{ansi_reset} ] "{vm}": Adding a SATA storage controller')
	os.system(f'{vboxmanage} storagectl "{vm}" --add sata --controller IntelAHCI --name "SATA" --portcount 3 --hostiocache off')

	print(f'[ {ansi_green}INFO{ansi_reset} ] "{vm}": Adding an empty DVD drive')
	os.system(f'{vboxmanage} storageattach "{vm}" --storagectl "SATA" --port 0 --type dvddrive --medium emptydrive')


	print(f'[ {ansi_green}INFO{ansi_reset} ] "{vm}": Adding hard drives')
	print(f'\t{basedir}{separator}HDD{separator}{vm}.vdi')
	os.system(f'{vboxmanage} storageattach {vm} --storagectl "SATA" --port 1 --type hdd --medium {basedir}{separator}HDD{separator}{vm}.vdi')
	if vm == 'LAMP1':
		for i in [1, 2]:
			print(f'\t{basedir}{separator}HDD{separator}RAID-Disk-0{i}.vdi')
			os.system(f'{vboxmanage} storageattach {vm} --storagectl "SATA" --port {i+1} --type hdd --medium {basedir}{separator}HDD{separator}RAID-Disk-0{i}.vdi')

	## Network
	for vm in vms:
		print(f'[ {ansi_green}INFO{ansi_reset} ] "{vm}": Setting up network')
		if vm == 'R1':
			os.system(f'{vboxmanage} modifyvm "{vm}" --nic1 nat')
			os.system(f'{vboxmanage} modifyvm "{vm}" --nic2 intnet --intnet2 "WAN"')
		if vm == 'FW1':
			os.system(f'{vboxmanage} modifyvm "{vm}" --nic1 intnet --intnet1 "WAN"')
			os.system(f'{vboxmanage} modifyvm "{vm}" --nic2 intnet --intnet2 "Campus"')
			os.system(f'{vboxmanage} modifyvm "{vm}" --nic3 intnet --intnet3 "Datacenter"')
		if vm == 'LAMP1' or vm == 'INFRA1':
			os.system(f'{vboxmanage} modifyvm "{vm}" --nic1 intnet --intnet1 "Datacenter"')


	# os.system( f'{vboxmanage} startvm "{vm}" --type emergencystop')

os.environ['PATH'] = str( oldpath )


sys.exit(0)
