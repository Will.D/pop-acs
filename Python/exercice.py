#! /usr/bin/env python3

## Consignes

## DEBUT DE PARTIE
# Nombre user (nbuser) = 101
# Générer un nombre (nbmystere)
## BOUCLE DE LA PARTIE
# Tant que (nbmystere != nbuser)
# 	Demander la saisie d'un nombre (nbuser)
# 	Comparer les nombre
# 	Si nbuser > nbmystere
#		Afficher "C'est moins"
# 	Si nbuser < nbmystere
#		Afficher "C'est plus"
# 	Sinon
#		Afficher "Bravo"
# Fin Tant que


## Pour installer le module readchar, utilisez pip
	# pip install readchar




import sys
from getpass import getpass
from random import randint
from readchar import readchar


MIN = 1
MAX = 10

def print_value_error():
    print(f'\t> Veuillez saisir un nombre entier entre {MIN} et {MAX}', file=sys.stderr)

def new_game(players:list):
	## Configure players
	for player in players:
		player['name'] = input(f"\t> Joueur {player['index']}, quel est votre nom ? ")

	## Start game
	winner = False
	nbMystere = randint(MIN, MAX)
	while winner == False:
		for player in players:
			## Get value
			player['number'] = getpass(prompt=f"\t> {player['name']}, quel est le nombre mystère ? ")

			## Check value
			if not player['number'].isnumeric:
				print_value_error()
				continue
			elif int(player['number']) < MIN or int(player['number']) > MAX:
				print_value_error()
				continue

			## Test value
			if int(player['number']) > nbMystere:
				print('\tC\'est moins\n')
			elif int(player['number']) < nbMystere:
				print('\tC\'est plus\n')
			else:
				print(f'\tBravo, le nombre mystère est {nbMystere}\n')
				print(f"\t{player['name']} a gagné !")
				winner = True
				break
	return


def print_main_menu():
	print()
	print( "\tBienvenue dans mystery" )
	print( "\t----------------------" )
	print( "\tN - Nouvelle partie")
	print( "\tQ - Quitter")

def print_new_game_menu():
	print( '\n\tNouvelle partie' )
	print( '\t---------------' )
	print( '\t1 - Un Joueur' )
	print( '\t2 - Deux Joueurs' )
	print( '\tM - Menu Principal' )

def yes_no():
	while True:
		choice = readchar().upper()
		if choice == 'Y':
			return True
		elif choice == 'N':
			return False
		else:
			choice = ''
			continue







while True:
	choice = ''
	players = []
	print_main_menu()
	while choice == '':
		choice = str(readchar()).upper()

	if choice == 'N':
		print_new_game_menu()
		choice = ''
		while choice == '':
			choice = str(readchar()).upper()
			if choice == '1':
				player1 = {'index': 1, 'name': '', 'tries': 3, 'number': ''}
				players.append(player1)
				new_game(players)
			if choice == '2':
				player1 = {'index': 1, 'name': '', 'tries': 3, 'number': ''}
				player2 = {'index': 2, 'name': '', 'tries': 3, 'number': ''}
				players.append(player1)
				players.append(player2)
				new_game(players)
			if choice == 'M':
				choice = ''
				break

	if choice == 'Q':
		sys.exit(0)
