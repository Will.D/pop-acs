#! /usr/bin/env python3

# DRY - Don't Repeat Yourself

# lowercase:			toto
# uppercase:			TOTO		-> Constantes
# camel case:			monToto		-> Variables ou fonctions
# snake case:			mon_toto	-> Variables ou fonctions
# upper camel case:		MaClasse	-> Réservé à l'objet


maVariable = 'toto'

## Définition/Déclaration (création de la fonction)
def ma_fonction():
    pass

## Fonction avec passage d'argument (obligatoire)
def arguments(nom):
    print(nom)

## Fonction avec passage d'argument (et valeur par défaut)
def arguments(nom='Toto'):
    print(nom)

## Fonctions avec retour
def retour():
    return 1 + 1

print(retour())

def retour():
    return "Hello"

print(retour())

## Exemple de fonction
def square(number=1):
    return number*number

monNombre = 8
monCarre = square(monNombre)

print(monCarre)

