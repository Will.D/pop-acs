#! /usr/bin/env python3



def rotate(message,offset):
	alphaLower = 'abcdefghijklmnopqrstuvwxyz'[::-1]
	alphaUpper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[::-1]
	messageList = []

	for char in message:
		if char.islower():
			messageList.append(alphaLower[alphaLower.index(char)-offset])
		elif char.isupper():
			messageList.append(alphaUpper[alphaUpper.index(char)-offset])
		else:
			messageList.append(char)
	return ''.join(messageList)


message = 'Ceci est un secret'
message = 'Prpv rfg ha frperg'

offset = 13

print( rotate(message, offset) )
