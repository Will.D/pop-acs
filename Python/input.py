#! /usr/bin/env python3

nom = str()
age = int()

nom = input('Comment vous appelez-vous ? ')
age = input('Quel age avez-vous ? ')

print("Vous vous appelez " + nom)
print("Vous avez " + age + " ans")

print("L'année prochaine, vous aurez " + str(int(age) + 1) + " ans")
print(f"L'année prochaine, vous aurez {int(age) + 1} ans")

