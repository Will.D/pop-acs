#! /usr/bin/env python3


import sys
from mystery import *


while True:
	choice = main_menu()
	if choice == 'Q':
		sys.exit(0)
	elif choice == 'N':
		choice = new_game_menu()
		if choice.isnumeric():
			players = Players()
			player1 = Player()
			players.append(player1)
			if choice == '2':
				player2 = Player()
				players.append(player2)
			new_game(players)
			del players

		elif choice == 'M':
			continue
