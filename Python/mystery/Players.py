#! /usr/bin/env python3

class Players():
	__listing = list()
	__index = int()

	def __init__(self, listing = [], index = 0):
		self.setlisting(listing)
		self.setindex(index)

	def __del__(self):
		self.reset()


	def getlisting(self):
		return self.__listing

	def setlisting(self,listing):
		self.__listing = listing

	def getindex(self):
		return self.__index

	def setindex(self,index):
		self.__index = index



	def incindex(self):
		self.__index += 1

	def append(self, player):
		self.incindex()
		player.setindex(index=self.__index)
		self.__listing.append(player)

	def getplayerbyindex(self,index):
		for player in self.__listing:
			if player.getindex() == index:
				return player

	def reset(self):
		self.__listing.clear()
		self.setindex(0)