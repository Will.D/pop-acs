#! /usr/bin/env python3

import sys
from getpass import getpass
from random import randint
from readchar import readchar
from mystery import Player,Players

MIN = 1
MAX = 10

def print_value_error(min:int,max:int):
    print(f'\t> Veuillez saisir un nombre entier entre {min} et {max}', file=sys.stderr)

def print_main_menu():
	print()
	print( "\tBienvenue dans mystery" )
	print( "\t----------------------" )
	print( "\tN - Nouvelle partie")
	print( "\tQ - Quitter")

def print_new_game_menu():
	print( '\n\tNouvelle partie' )
	print( '\t---------------' )
	print( '\t1 - Un Joueur' )
	print( '\t2 - Deux Joueurs' )
	print( '\tM - Menu Principal' )

def yes_no():
	while True:
		choice = readchar().upper()
		if choice == 'Y':
			return True
		elif choice == 'N':
			return False
		else:
			choice = ''
			continue

def prompt_get_player_name(playerIndex:int):
	return input(f"\t> Joueur {playerIndex}, quel est votre nom ? ")

def prompt_get_player_number(playerIndex:int):
	return getpass(prompt=f"\t> Joueur {playerIndex}, quel est le nombre mystère ? ")

## Game loop
def new_game(players:Players):

	for player in players.getlisting():
		player.setname( prompt_get_player_name( player.getindex() ) )
		player.settries(3)

	## Start game
	winner = False
	mystery = randint(MIN, MAX)
	while winner == False:
		for player in players.getlisting():
			number = prompt_get_player_number( player.getindex() )

			## Check value
			if not number.isnumeric():
				print_value_error(MIN,MAX)
				continue
			else:
				player.setnumber( int(number) )
				if player.getnumber() < MIN or player.getnumber() > MAX:
					print_value_error(MIN,MAX)
					continue

			## Test value
			if player.getnumber() > mystery:
				print('\tC\'est moins\n')
			elif player.getnumber() < mystery:
				print('\tC\'est plus\n')
			else:
				print(f'\tBravo, le nombre mystère est {mystery}\n')
				print(f"\t{player.getname()} a gagné !")
				winner = True
				break
	return

def main_menu():
	choice = ''
	print_main_menu()
	while choice == '':
		choice = str( readchar().upper() )
		if choice in ['N','Q']:
			return choice
		else:
			choice = ''

def new_game_menu():
	choice = ''
	print_new_game_menu()
	while choice == '':
		choice = str( readchar().upper() )
		if choice in ['1','2','M']:
			return choice
		else:
			choice = ''
