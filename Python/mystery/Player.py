#! /usr/bin/env python3

class Player():
	__index = int()
	__name = str()
	__tries = int()
	__number = int()

	## Constructeur
	def __init__(self, name='', tries=0, index=0, number=0):
		self.setname(name)
		self.settries(tries)
		self.setindex(index)
		self.setnumber(number)


	## index
	def getindex(self):
		return self.__index

	def setindex(self,index):
		self.__index = index

	## name
	def getname(self):
		return self.__name

	def setname(self, name):
		self.__name = name

	## tries
	def gettries(self):
		return self.__tries

	def settries(self, tries):
		self.__tries = tries

	## number
	def setnumber(self,number):
		self.__number = number

	def getnumber(self):
		return self.__number


	def dectries(self):
		if self.__tries > 0:
			self.__tries -= 1

	def dectries(self):
		self.__tries += 1

	def print(self):
		print(self)
		print(f'name: {self.__name}')
		print(f'tries: {self.__tries}')
		print(f'index: {self.__index}')
